import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  ${({ theme }) => css`
    margin-top: 5rem;
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 2rem;

    select {
      font-size: ${theme.font.sizes.size20PX};
    }
  `}
`
