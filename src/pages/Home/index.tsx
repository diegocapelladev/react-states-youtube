import { ChangeEvent, useState } from 'react'
import { useCidades } from '../../hooks/useCidade'
import { useEstados } from '../../hooks/useEstados'
import * as S from './styles'

export const Home = () => {
  const [selectedEstado, setSelectedEstado] = useState('')
  const { estados } = useEstados()
  const { cidades, loading: loadingCidades } = useCidades({
    uf: selectedEstado
  })

  const handleEstadoUpdate = (event: ChangeEvent<HTMLSelectElement>) => {
    setSelectedEstado(event.target.value)
  }

  return (
    <S.Wrapper>
      <select value={selectedEstado} onChange={handleEstadoUpdate}>
        {estados.map((estado) => (
          <option key={estado.id} value={estado.sigla}>
            {estado.nome}
          </option>
        ))}
      </select>

      {loadingCidades ? (
        <p>Loading...</p>
      ) : (
        <select>
          {cidades.map((cidade) => (
            <option key={cidade.codigo_ibge}>{cidade.nome}</option>
          ))}
        </select>
      )}
    </S.Wrapper>
  )
}
