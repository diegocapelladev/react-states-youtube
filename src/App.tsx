import { Container } from './components/Container'
import { Router } from './Routes'

export function App() {
  return (
    <Container>
      <Router />
    </Container>
  )
}
