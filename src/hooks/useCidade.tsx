import { useEffect, useState } from 'react'

export interface Cidades {
  nome: string
  codigo_ibge: string
}

export interface CidadeProps {
  uf: string
}

export const useCidades = ({ uf }: CidadeProps) => {
  const [cidades, setCidades] = useState<Cidades[]>([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (!uf) return

    setLoading(true)

    fetch(`https://brasilapi.com.br/api/ibge/municipios/v1/${uf}`)
      .then((response) => response.json())
      .then((data) => setCidades(data))
      .then(() => setLoading(false))
  }, [uf])

  return {
    cidades,
    loading
  }
}
